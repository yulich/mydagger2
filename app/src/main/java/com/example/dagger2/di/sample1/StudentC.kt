package com.example.dagger2.di.sample1

import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

class StudentC(score: Number) {
    init {
        DaggerStudentCComponent.builder().studentCModule(StudentCModule(score)).build().injectStudent(this)
    }

    /**
     * Inject：用來標註需要注入的目標，標註在變數上產生 _MembersInjector，而標註在建構子上會為該目標產生 _Factory 類別。
     */
    @Inject
    lateinit var test1: SingletonTest

    @Inject
    lateinit var test2: SingletonTest

    @Inject
    lateinit var test3: NotSingletonTest

    @Inject
    lateinit var test4: NotSingletonTest
}

class SingletonTest @Inject constructor(private val score: Number) {

}

/**
 * 假設為 data class 會因為數據相同，兩個物件相同.
 */
class NotSingletonTest @Inject constructor(private val score: Number) {

}

/**
 * Module：非必要，當依賴對象需要較複雜的產生方式時，可以藉由宣告 Module 類別提供更詳細的建構細節。
 */
@Module
class StudentCModule(private val score: Number) {

    /**
     * Provides：在 Module 中可以根據目標依賴對象的類別宣告產生函式，會產生 StudentBModule_ProvideTest1Factory 類別。
     * 相同類別只能存在一個。
     */
    @Singleton
    @Provides
    fun provideSingleton() = SingletonTest(score)

    @Provides
    fun provideNotSingleton() = NotSingletonTest(score)
}

@Singleton
@Component(modules = [StudentCModule::class])
interface StudentCComponent {
    fun injectStudent(studentC: StudentC)
}