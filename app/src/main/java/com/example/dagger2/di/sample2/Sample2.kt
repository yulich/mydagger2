package com.example.dagger2.di.sample2

import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

class Sample2 {

    @Inject
    lateinit var car: Car

    @Inject
    lateinit var singletonCar: SingletonCar
}

class Car @Inject constructor()

class SingletonCar @Inject constructor()

@Module
class Sample2Module {

    @Provides
    fun provideCar() = Car()

    @Singleton
    @Provides
    fun provideSingleTon() = SingletonCar()
}

@Singleton
@Component(modules = [Sample2Module::class])
interface Sample2Component {
    fun injectSample2(sample2: Sample2)
}
