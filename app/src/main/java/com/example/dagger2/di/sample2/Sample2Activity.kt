package com.example.dagger2.di.sample2

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.dagger2.DebugConfig.Companion.LOG_TAG
import com.example.dagger2.R
import com.example.dagger2.databinding.ActivityMainBinding

class Sample2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.tv1.text = "Sample2"

        val sample2Fist = Sample2()
        val sample2Second = Sample2()

        val module = Sample2Module()

        val component1 = DaggerSample2Component.builder()
                .sample2Module(module)
                .build()

        // 必須是相同的component，singleton才能發揮效果.
        /*
        val component2 = DaggerSample2Component.builder()
            .sample2Module(module)
            .build()
            */

        component1.injectSample2(sample2Fist)
        component1.injectSample2(sample2Second)

        if (sample2Fist.car == sample2Second.car) {
            Log.d(LOG_TAG, "Car is equal")
        } else {
            Log.d(LOG_TAG, "Car is not equal")
        }

        if (sample2Fist.singletonCar == sample2Second.singletonCar) {
            Log.d(LOG_TAG, "SingletonCar is equal")
        } else {
            Log.d(LOG_TAG, "SingletonCar is not equal")
        }
    }
}