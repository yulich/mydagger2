package com.example.dagger2.di.sample1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.example.dagger2.DebugConfig.Companion.LOG_TAG
import com.example.dagger2.R
import com.example.dagger2.databinding.ActivityMainBinding

class Sample1Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.tv1.text = "Sample1"

        StudentA().also { Log.d(LOG_TAG, "${it.mathTest}") }

        StudentB().also { Log.d(LOG_TAG, "${it.mathTest}, ${it.englishTest}") }

        // Singleton測試, 在物件中有兩組兩兩相同的類別, 其中一組類別有標註Singleton.
        // 結果有標註Singleton的類別, 取得的物件是相同的.
        StudentC(60).also {
            Log.d(LOG_TAG, "${it.test1}, ${it.test2}")
            if (it.test1 == it.test2) {
                Log.d(LOG_TAG, "test1 == test2")
            } else {
                Log.d(LOG_TAG, "test1 != test2")
            }

            Log.d(LOG_TAG, "${it.test3}, ${it.test4}")
            if (it.test3 == it.test4) {
                Log.d(LOG_TAG, "test3 == test4")
            } else {
                Log.d(LOG_TAG, "test3 != test4")
            }
        }
    }
}
