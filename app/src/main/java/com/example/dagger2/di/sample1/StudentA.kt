package com.example.dagger2.di.sample1

import dagger.Component
import javax.inject.Inject

class StudentA {
    init {
        DaggerStudentAComponent.create().injectStudent(this)
    }

    /**
     * Inject：用來標註需要注入的目標，標註在變數上產生 _MembersInjector 而標註在建構子上會為該目標產生 _Factory 類別。
     */
    @Inject
    lateinit var mathTest: MathTest
}

data class MathTest(val score: Number) {

    /**
     * 沒有帶參數時，預設建構式。
     */
    @Inject
    constructor() : this(60)
}

/**
 * Component：用來連結目標與依賴成員的媒介，我們只需要定義 interface ， Dagger 會產生注入的實作。
 */
@Component
interface StudentAComponent {
    fun injectStudent(studentA: StudentA)
}