package com.example.dagger2.di.component

import com.example.dagger2.MyApplication
import com.example.dagger2.di.module.ApiServiceModule
import com.example.dagger2.di.module.AppModule
import com.example.dagger2.di.module.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class, ApiServiceModule::class])
interface AppComponent {
    fun inject(app: MyApplication)
}