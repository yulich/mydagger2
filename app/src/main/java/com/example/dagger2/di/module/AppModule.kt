package com.example.dagger2.di.module

import android.content.Context
import com.example.dagger2.MyApplication
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

@Module
class AppModule @Inject constructor(private val application: MyApplication) {

    @Singleton
    @Provides
    fun provideApplication() = application

    @Singleton
    @Provides
    fun provideContext(): Context = application.applicationContext
}