package com.example.dagger2.di.sample1

import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Inject

class StudentB {
    init {
        DaggerStudentBComponent.builder().studentBModule(StudentBModule(80)).build().injectStudent(this)
    }

    /**
     * Inject：用來標註需要注入的目標，標註在變數上產生 _MembersInjector，而標註在建構子上會為該目標產生 _Factory 類別。
     */
    @Inject
    lateinit var mathTest: MathTest

    @Inject
    lateinit var englishTest: EnglishTest
}

data class EnglishTest @Inject constructor(val score: Number) {
}

/**
 * Module：非必要，當依賴對象需要較複雜的產生方式時，可以藉由宣告 Module 類別提供更詳細的建構細節。
 */
@Module
class StudentBModule(private val mathScore: Number) {

    /**
     * Provides：在 Module 中可以根據目標依賴對象的類別宣告產生函式，會產生 StudentBModule_ProvideTest1Factory 類別。
     * 相同類別只能存在一個。
     */
    @Provides
    fun provideTest1() = MathTest(mathScore)

    /**
     * 參數會去找到已經產生的物件
     */
    @Provides
    fun provideTest2(test: MathTest) = EnglishTest(test.score)
}

@Component(modules = [StudentBModule::class])
interface StudentBComponent {
    fun injectStudent(studentB: StudentB)
}