package com.example.dagger2.api

import com.example.dagger2.api.bean.ProfileBean
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("profile")
    fun getProfileRx(): Single<Response<ProfileBean>>
}