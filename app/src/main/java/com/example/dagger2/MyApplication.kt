package com.example.dagger2

import android.app.Application
import android.util.Log
import com.example.dagger2.api.ApiService
import com.example.dagger2.api.bean.ProfileBean
import com.example.dagger2.di.component.AppComponent
import com.example.dagger2.di.component.DaggerAppComponent
import com.example.dagger2.di.module.AppModule
import com.example.dagger2.di.module.NetworkModule
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class MyApplication : Application() {

    @Inject
    lateinit var retrofit: Retrofit

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var apiService: ApiService

    private val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .networkModule(NetworkModule("https://my-json-server.typicode.com/typicode/demo/"))
            .build()
    }

    fun getDefaultComponent() = appComponent

    override fun onCreate() {
        super.onCreate()

        getDefaultComponent().inject(this)

        singleRx1()
    }

    private fun singleRx1() {
        apiService.getProfileRx().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : DisposableSingleObserver<Response<ProfileBean>>() {
            override fun onSuccess(response: Response<ProfileBean>) {
                val code = response.code()
                val commentsBean = response.body()
                if (commentsBean != null) {
                    Log.d(DebugConfig.LOG_TAG, "$commentsBean")
                }
            }

            override fun onError(e: Throwable) {
                Log.d(DebugConfig.LOG_TAG, "OnError: ${e.message}")
            }
        })
    }
}